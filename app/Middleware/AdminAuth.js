class AdminAuth {
    beforeLogin(req, res) {
        try {
            if (req.session.admin.id != '') {
                res.redirect('/admin/dashboard/');
            } else {
                throw 'Admin Session Not Exist';
            }
        } catch (error) {
            return true;
        }
    }

    afterLogin(req, res, next) {
        try {
            if (req.session.admin == null) {
                res.redirect('/admin/login/');
            } else {
                throw 'Admin Session Exist';
            }
        } catch (error) {
            next();
        }
    }
}

module.exports = AdminAuth;