const subdomain = _use('express-subdomain');
const helmet = _use('helmet');
const csrf = _use('csurf');

/*--------------------------------------------------------------------------
 * Routes
 */
module.exports = (app, viewEnv) => {
    /**
     * Routs
     */
    const api = _use('Routes/Api');
    const admin = _use('Routes/Admin');
    const index = _use('Routes/Index');

    // Controller.
    let Controller = _use(`Controller/Var/Controller`);
    Controller = new Controller();


    // Middleware
    app.use((req, res, next) => {
        _log("\n****************************************************\n");
        _log(req.body);
        _log(req.query);
        _log(req.params);
        _log(req.originalUrl);
        next();
    });
    app.use(helmet());


    /*--------------------------------------------------------------------------
     * Routes
     */
    app.use('/api/', api);
    app.use(csrf());
    app.use((req, res, next) => {
        _use('Lib/Nunjuck', viewEnv, req, res);
        next();
    });
    app.use('/admin/', admin);
    app.use('/', index);


    /*--------------------------------------------------------------------------
     * APP Error Pages
     */
    // catch 404 and forward to error handler
    app.use((req, res, next) => {
        var err = new Error('Not Found');
        err.status = 404;
        Controller._page404(res);
        // next(err);
    });

    // error handler
    app.use((err, req, res, next) => {
        // set locals, only providing error in development
        res.locals.message = err.message;
        res.locals.error = req.app.get('env') === 'development' ? err : {};

        // render the error page
        res.status(err.status || 500);
        Controller._page500(res);
        // res.render('error');
    });

    // Subdomain
    // app.use(subdomain('api', api));
};