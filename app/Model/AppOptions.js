class AppOptions {
    constructor() {
        _private.self = this;
        this.model = $db.app_option;
    }
}

// Private Method
let _private = {
    options: [
        'admin_type_json',
        'admin_status_json'
    ]
};

module.exports = AppOptions;