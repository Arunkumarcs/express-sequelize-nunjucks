class AppAdminOptions {
    constructor() {
        this.model = $db.app_admin_options;
    }

    /**
     * Boot Options
     */
    async boot(admin_id) {
        this.load = await this.find(
            admin_id,
            '', [
                'option_name', 'option_value'
            ]
        );
        return this.load;
    }

    /**
     * Get all Admin
     */
    async find(admin_id, option_name = '', attributes = '') {
        let ret = [];
        let where = { admin_id };

        if (option_name != '') {
            where.option_name = option_name
        }

        if (attributes != '') {
            ret = await this.model.findAll({
                attributes,
                where,
                raw: true,
            });
        } else {
            ret = await this.model.findAll({
                where,
                raw: true,
            });
        }
        return ret;
    }

    /**
     * Getter and Setter
     */
    get load() {
        return _private.load
    }
    set load(data) {
        _private.load = data;
    }
}

// Private Method
let _private = {
    load: {},
    options: [
        'type',
        'status'
    ]
};

module.exports = AppAdminOptions;