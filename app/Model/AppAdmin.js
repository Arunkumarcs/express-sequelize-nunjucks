let encDec = _use('Lib/EncDecLib');
encDec = new encDec();

/**
 * Models
 */
let AppAdminOptions = _use(`Model/AppAdminOptions`);
AppAdminOptions = new AppAdminOptions();

class AppAdmin {
    constructor() {
        this.model = $db.app_admin;
    }

    /**
     * Boot User
     */
    async boot(id) {
        this.load = await this.findById(
            id, [
                'name', 'email', 'id'
            ]
        );

        // loading options
        if (this.load.id != undefined) {
            this.load.options = await AppAdminOptions.boot(this.load.id);
        }
        return this.load;
    }

    /**
     * Get all Admin
     */
    async findAll() {
        let ret = await this.model.findAll({
            raw: true,
        });
        return ret;
    }

    /**
     * Find by Id
     */
    async findById(id, attributes = '') {
        let ret = [];
        let where = { id };

        if (attributes != '') {
            ret = await this.model.findOne({
                attributes,
                where,
                raw: true,
            });
        } else {
            ret = await this.model.findOne({
                where,
                raw: true,
            });
        }
        return ret;
    }

    /**
     * Find Admin by Email
     */
    async findByEmail(email) {
        return await this.model.findOne({
            where: { email },
            raw: true
        });
    }

    /**
     * Create new Admin
     * @param {*} email 
     * @param {*} password 
     */
    async create(email, password) {
        password = encDec.encrypt(
            password,
            encDec._makeid()
        );

        return await this.model.findOrCreate({
            where: { email: email },
            defaults: {
                email: email,
                name: 'admin',
                password: password.data,
                key: password.key
            }
        });
        // .spread((user, created) => {
        //     callback(
        //         user.get({
        //             plain: true
        //         }),
        //         created
        //     );
        // });
    }

    /**
     * Getter and Setter
     */
    get load() {
        return _private.load
    }
    set load(data) {
        _private.load = data;
    }
}

// Private Method
let _private = {
    load: {}
};

module.exports = AppAdmin;