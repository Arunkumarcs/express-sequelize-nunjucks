const express = require('express');
const cors = _use('cors');
const router = express.Router();

// Middleware
router.use((req, res, next) => {
    // _log("\n*****************************************************\n");
    next();
});
router.use(cors());

module.exports = router;