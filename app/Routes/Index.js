const express = require('express');
const router = express.Router();

// Controller.
const Controller = _use(`Controller/Var/Controller`);
let ControllerObj = new Controller();

// Middleware
router.use((req, res, next) => {
    // _log("\n*****************************************************\n");
    next();
});

// Web Routes
router.get('/', (req, res) => {
    ControllerObj.index(req, res);
});

module.exports = router;