const express = require('express');
const router = express.Router();

// Controller.
let AdminLogin = _use(`Controller/Admin/Login`);
let AdminDashboard = _use(`Controller/Admin/Dashboard`);
let AdminCms = _use(`Controller/Admin/Cms`);
AdminLogin = new AdminLogin();
AdminDashboard = new AdminDashboard();
AdminCms = new AdminCms();

// Middlewares
let AdminAuth = _use(`Middleware/AdminAuth`);
AdminAuth = new AdminAuth();

// Middleware
router.use((req, res, next) => {
    // _log("\n****************************************************\n");
    // _log(req.body);
    // _log(req.query);
    // _log(req.params);
    // _log(req.originalUrl);
    next();
});

/*******************************************************************************
 * Before Login
 */
router.get('/login/', (req, res) => {
    AdminLogin.login(req, res);
});
router.post('/login/validate/', (req, res) => {
    AdminLogin.validate(req, res);
});

/*******************************************************************************
 * After Login
 */
router.use(AdminAuth.afterLogin);
router.get('/logout/', (req, res) => {
    AdminDashboard.logout(req, res);
});
router.get('/dashboard/', (req, res) => {
    AdminDashboard.index(req, res);
});

/*******************************************************************************
 * CMS Pages
 * FAQ
 */
router.get('/cms/faq/', (req, res) => {
    AdminCms.faq(req, res);
});
router.get('/cms/faq/new/', (req, res) => {
    AdminCms.faqNew(req, res);
});
router.post('/cms/faq/new/', (req, res) => {
    AdminCms.faqNew(req, res);
});
router.get('/cms/faq/edit/:blogId', (req, res) => {
    AdminCms.faqEdit(req, res);
});
router.post('/cms/faq/edit/:blogId', (req, res) => {
    AdminCms.faqEdit(req, res);
});

/**
 * Pages
 */
router.get('/cms/pages/', (req, res) => {
    AdminCms.pages(req, res);
});
router.get('/cms/pages/new/', (req, res) => {
    AdminCms.pagesNew(req, res);
});
router.post('/cms/pages/new/', (req, res) => {
    AdminCms.pagesNew(req, res);
});
router.get('/cms/pages/edit/:blogId', (req, res) => {
    AdminCms.pagesEdit(req, res);
});
router.post('/cms/pages/edit/:blogId', (req, res) => {
    AdminCms.pagesEdit(req, res);
});

/**
 * Images
 */
router.get('/cms/images/', (req, res) => {
    AdminCms.images(req, res);
});
router.get('/cms/images/new/', (req, res) => {
    AdminCms.imagesNew(req, res);
});
router.post('/cms/images/new/', (req, res) => {
    AdminCms.imagesNew(req, res);
});
router.get('/cms/images/edit/:blogId', (req, res) => {
    AdminCms.imagesEdit(req, res);
});
router.post('/cms/images/edit/:blogId', (req, res) => {
    AdminCms.imagesEdit(req, res);
});

/**
 * Categories
 */
router.get('/cms/categories/', (req, res) => {
    AdminCms.categories(req, res);
});
router.get('/cms/categories/new/', (req, res) => {
    AdminCms.categoriesNew(req, res);
});
router.post('/cms/categories/new/', (req, res) => {
    AdminCms.categoriesNew(req, res);
});
router.get('/cms/categories/edit/:blogId', (req, res) => {
    AdminCms.categoriesEdit(req, res);
});
router.post('/cms/categories/edit/:blogId', (req, res) => {
    AdminCms.categoriesEdit(req, res);
});

/**
 * Tags
 */
router.get('/cms/tags/', (req, res) => {
    AdminCms.tags(req, res);
});
router.get('/cms/tags/new/', (req, res) => {
    AdminCms.tagsNew(req, res);
});
router.post('/cms/tags/new/', (req, res) => {
    AdminCms.tagsNew(req, res);
});
router.get('/cms/tags/edit/:blogId', (req, res) => {
    AdminCms.tagsEdit(req, res);
});
router.post('/cms/tags/edit/:blogId', (req, res) => {
    AdminCms.tagsEdit(req, res);
});

/**
 * Settings
 */
router.get('/cms/setting/', (req, res) => {
    AdminCms.setting(req, res);
});

module.exports = router;