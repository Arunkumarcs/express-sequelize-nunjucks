let $env = require('../Config/Env.json');
let obj = [];

for (var key in $env) {
    obj.push({
        name: $env[key]["_pm2"]["name"],
        script: './bin/www',
        instances: $env[key]["_pm2"]["instances"],
        watch: $env[key]["_pm2"]["watch"],
        watch_options: {
            persistent: true
        },
        env: {
            "PORT": $env[key]["_pm2"]["port"],
            "NODE_ENV": $env[key]['name']
        },
        exec_mode: "cluster"
    });
}

module.exports = {
    /**
     * Application configuration section
     * http://pm2.keymetrics.io/docs/usage/application-declaration/
     */
    apps: obj
};