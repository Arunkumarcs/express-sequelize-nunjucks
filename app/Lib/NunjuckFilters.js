let _private = {
    shorten(str, count = 5) {
        return str.slice(0, count);
    }
};

module.exports = (viewEnv, req, res) => {
    _private.req = req;
    _private.res = res;
    viewEnv.addFilter('shorten', _private.shorten);
};