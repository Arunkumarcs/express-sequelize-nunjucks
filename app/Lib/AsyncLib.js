class AsyncLib {
    constructor() {}

    // Async Waterfall
    _asyncWaterfall(array, { req, res }, callback = '') {
        $async.waterfall(
            array,
            function(err, result) {
                if (typeof callback == 'function') {
                    callback(err, result, req, res);
                } else {
                    res.jsonp({
                        status: 'error',
                        error: err
                    });
                }
            }
        );
    }

    // Async Series
    _asyncSeries(obj, { req, res }, callback = '') {
        $async.series(
            obj,
            function(err, result) {
                if (typeof callback == 'function') {
                    callback(err, result, req, res);
                } else {
                    res.jsonp({
                        status: 'error',
                        error: err
                    });
                }
            }
        );
    }

    // Async Parallel
    _asyncParallel(obj, { req, res }, callback = '') {
        $async.parallel(
            obj,
            function(err, result) {
                if (typeof callback == 'function') {
                    callback(err, result, req, res);
                } else {
                    res.jsonp({
                        status: 'error',
                        error: err
                    });
                }
            }
        );
    }
}

module.exports = AsyncLib;