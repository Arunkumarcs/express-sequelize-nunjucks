// Public packages.
const cryptoJs = _use('crypto-js');

class EncDecLib {
    constructor() {
        this.key = $config.env.secret;
    }

    _makeid() {
        let text = "";
        let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~!@#$%^&*()_+-={}[]|\\;:'\",.?/";

        for (let i = 0; i < 5; i++) {
            text += possible.charAt(
                Math.floor(Math.random() * possible.length)
            );
        }
        return text;
    }

    // Encryption
    encrypt(value, key = '') {
        if (key == '') {
            key = this._makeid();
        }
        return {
            data: cryptoJs.AES.encrypt(
                value,
                this.key + key
            ).toString(),
            key: key
        };
    }

    // Decryption
    decrypt(value, key = '') {
        return {
            data: cryptoJs.AES.decrypt(
                value,
                this.key + key
            ).toString(cryptoJs.enc.Utf8),
            key: key
        }
    }
}

module.exports = EncDecLib;