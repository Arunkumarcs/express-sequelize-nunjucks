let _private = {};

module.exports = (viewEnv, req, res) => {
    _private.req = req;
    _private.res = res;
    _use('Lib/Nunjuck/Filters', viewEnv, req, res);
    _use('Lib/Nunjuck/Tags', viewEnv, req, res);
    _use('Lib/Nunjuck/Globals', viewEnv, req, res);
};