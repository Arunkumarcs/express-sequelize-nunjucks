// Require `Nuxt` And `Builder` modules
const { Nuxt, Builder } = _use('nuxt');

// Require Nuxt config
const config = {
    srcDir: 'resources/Nuxt'
};

// Create a new Nuxt instance
$nuxt = new Nuxt(config);

// Enable live build & reloading on dev
if ($nuxt.options.dev) {
    new Builder($nuxt).build();
}

// We can use `$nuxt.render(req, res)` or `$nuxt.renderRoute(route, context)`