const expressSession = _use('express-session');
const SessionStore = _use('express-session-sequelize')(expressSession.Store);
const sequelizeSessionStore = new SessionStore({
    db: $db.sequelize
});


/*
 * Session storage & Session Initialize..
 */
module.exports = (app) => {
    app.use(expressSession({
        secret: 'keep it secret, keep it safe.jki768',
        store: sequelizeSessionStore,
        resave: false,
        saveUninitialized: false,
    }));
};