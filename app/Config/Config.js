/*
 * Configurations.
 */
$env = process.env.NODE_ENV;
$env = $env.trim();
$config = {};
$db = {};
console.log('App runing in "' + $env + '" Environment.\n');


/*
 * Initialize Use Helper, Env Config & Initialize sequelize ORM
 */
require('ak');
$config.env = _use('Config/Env.json')[$env];
$debug = $config.env['debug'];
$db = _use('BASE/db/models/index.js');


/*
 * Initialize Autoload, ORM association, Session and Routes.
 * _use('Config/_Nuxt');
 */
module.exports = (app, viewEnv) => {
    _use('Config/Autoload', $config, $db);
    _use('Model/Autoload', $config, $db);
    _use('Config/Session', app);
    _use('APP/Routes', app, viewEnv);
};