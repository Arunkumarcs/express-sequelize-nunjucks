class Controller extends $Controller {
    _properties() { this._theme = 'AdminLTE'; }
    _init() {}

    async index(req, res) {
        this._render('index', { _pageName: 'home' }, res);
    }
}

module.exports = Controller;