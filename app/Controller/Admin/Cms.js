const Dashboard = _use(`Controller/Admin/Dashboard`);

class Cms extends Dashboard {
    /**
     * Pages
     * @param {*} req 
     * @param {*} res 
     */
    async pages(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogManage', {
            user: this.admin,
            page: {
                title: 'Blog Pages',
                addNew: '/admin/cms/pages/new/'
            }
        }, res);
    }
    async pagesNew(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogEdit', {
            user: this.admin,
            page: {
                title: 'Add Page'
            }
        }, res);
    }
    async pagesEdit(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogEdit', {
            user: this.admin,
            page: {
                title: 'Edit Page'
            }
        }, res);
    }

    /**
     * Images
     * @param {*} req 
     * @param {*} res 
     */
    async images(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogManage', {
            user: this.admin,
            page: {
                title: 'Blog Images',
                addNew: '/admin/cms/images/new/'
            }
        }, res);
    }
    async imagesNew(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogEdit', {
            user: this.admin,
            page: {
                title: 'Add Images'
            }
        }, res);
    }
    async imagesEdit(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogEdit', {
            user: this.admin,
            page: {
                title: 'Edit Images'
            }
        }, res);
    }

    /**
     * FAQ
     * @param {*} req 
     * @param {*} res 
     */
    async faq(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogManage', {
            user: this.admin,
            page: {
                title: 'FAQ',
                addNew: '/admin/cms/faq/new/'
            }
        }, res);
    }
    async faqNew(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogEdit', {
            user: this.admin,
            page: {
                title: 'Add FAQ'
            }
        }, res);
    }
    async faqEdit(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogEdit', {
            user: this.admin,
            page: {
                title: 'Edit FAQ'
            }
        }, res);
    }

    /**
     * Categories
     * @param {*} req 
     * @param {*} res 
     */
    async categories(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogManage', {
            user: this.admin,
            page: {
                title: 'Blog Categories',
                addNew: '/admin/cms/categories/new/'
            }
        }, res);
    }
    async categoriesNew(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogEdit', {
            user: this.admin,
            page: {
                title: 'Add Categories'
            }
        }, res);
    }
    async categoriesEdit(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogEdit', {
            user: this.admin,
            page: {
                title: 'Edit Categories'
            }
        }, res);
    }

    /**
     * Tags
     * @param {*} req 
     * @param {*} res 
     */
    async tags(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogManage', {
            user: this.admin,
            page: {
                title: 'Blog Tags',
                addNew: '/admin/cms/tags/new/'
            }
        }, res);
    }
    async tagsNew(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogEdit', {
            user: this.admin,
            page: {
                title: 'Add Tag'
            }
        }, res);
    }
    async tagsEdit(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogEdit', {
            user: this.admin,
            page: {
                title: 'Edit Tags'
            }
        }, res);
    }

    /**
     * Setting
     * @param {*} req 
     * @param {*} res 
     */
    async setting(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/dashboard', {
            user: this.admin,
            page: {
                title: 'Blog Setting'
            }
        }, res);
    }
}

module.exports = Cms;