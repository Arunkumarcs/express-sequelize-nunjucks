const Controller = _use(`Controller/Var/Controller`);

// Middlewares
let AdminAuth = _use(`Middleware/AdminAuth`);
AdminAuth = new AdminAuth();

// Model
let AdminModel = _use(`Model/AppAdmin`);
AdminModel = new AdminModel();

// Lib
let encDec = _use('Lib/EncDecLib');
encDec = new encDec();

class Login extends Controller {
    _init() {
        _private.self = this;
    }

    // Admin Login
    async login(req, res) {
        let auth = AdminAuth.beforeLogin(req, res);

        if (auth == true) {
            this._render(
                'admin/login', {
                    _pageName: 'login',
                    form: {
                        action: '/admin/login/validate/'
                    },
                    csrfToken: req.csrfToken()
                },
                res
            );
        }
    }

    // Validate Login Credentials
    async validate(req, res) {
        if (req.xhr) {
            let self = this;
            let admin = await AdminModel.findByEmail(req.body.email);

            try {
                if (typeof admin.id == 'null') {
                    throw 'Admin Not Exist';
                }

                let oldPas = encDec.decrypt(admin.password, admin.key);

                if (oldPas.data != req.body.password) {
                    throw 'Invalid Pasword';
                }

                _private.assignSession(admin, req);
                self._renderJ({
                    status: 'success'
                }, res);
            } catch (error) {
                self._renderJ({
                    status: "error",
                    message: error
                }, res);
            }
        } else {
            res.redirect('/admin/login/');
        }
    }

    // async create(req, res) {
    //     if (req.xhr) {
    //         let self = this;
    //         let admin = await AdminModel.create(
    //             req.body.email,
    //             req.body.password
    //         );
    //         _log(admin[1]);
    //         self._renderJ(req.body, res);
    //     } else {
    //         res.redirect('/admin/login/');
    //     }
    // }
}

// Private Method
let _private = {
    assignSession(admin, req) {
        req.session.admin = {
            id: admin.id,
            name: admin.name,
            email: admin.email
        };
    }
};

module.exports = Login;