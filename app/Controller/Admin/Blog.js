const Dashboard = _use(`Controller/Admin/Dashboard`);

class Blog extends Dashboard {
    /**
     * Pages
     * @param {*} req 
     * @param {*} res 
     */
    async pages(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogManage', {
            user: this.admin,
            page: {
                title: 'Blog Pages',
                addNew: '/admin/cms/pages/new/'
            },
            total: {
                faq: 1,
                blog: 2,
                users: 3,
                products: 4
            }
        }, res);
    }
    async pagesNew(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogEdit', {
            user: this.admin,
            page: {
                title: 'Add Page'
            },
            total: {
                faq: 1,
                blog: 2,
                users: 3,
                products: 4
            }
        }, res);
    }
    async pagesEdit(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogEdit', {
            user: this.admin,
            page: {
                title: 'Edit Page'
            },
            total: {
                faq: 1,
                blog: 2,
                users: 3,
                products: 4
            }
        }, res);
    }

    /**
     * Images
     * @param {*} req 
     * @param {*} res 
     */
    async images(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogManage', {
            user: this.admin,
            page: {
                title: 'Blog Images',
                addNew: '/admin/cms/images/new/'
            },
            total: {
                faq: 1,
                blog: 2,
                users: 3,
                products: 4
            }
        }, res);
    }
    async imagesNew(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogEdit', {
            user: this.admin,
            page: {
                title: 'Add Images'
            },
            total: {
                faq: 1,
                blog: 2,
                users: 3,
                products: 4
            }
        }, res);
    }
    async imagesEdit(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogEdit', {
            user: this.admin,
            page: {
                title: 'Edit Images'
            },
            total: {
                faq: 1,
                blog: 2,
                users: 3,
                products: 4
            }
        }, res);
    }

    /**
     * FAQ
     * @param {*} req 
     * @param {*} res 
     */
    async faq(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogManage', {
            user: this.admin,
            page: {
                title: 'FAQ',
                addNew: '/admin/cms/faq/new/'
            },
            total: {
                faq: 1,
                blog: 2,
                users: 3,
                products: 4
            }
        }, res);
    }
    async faqNew(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogEdit', {
            user: this.admin,
            page: {
                title: 'Add FAQ'
            },
            total: {
                faq: 1,
                blog: 2,
                users: 3,
                products: 4
            }
        }, res);
    }
    async faqEdit(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogEdit', {
            user: this.admin,
            page: {
                title: 'Edit FAQ'
            },
            total: {
                faq: 1,
                blog: 2,
                users: 3,
                products: 4
            }
        }, res);
    }

    /**
     * Categories
     * @param {*} req 
     * @param {*} res 
     */
    async categories(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogManage', {
            user: this.admin,
            page: {
                title: 'Blog Categories',
                addNew: '/admin/cms/categories/new/'
            },
            total: {
                faq: 1,
                blog: 2,
                users: 3,
                products: 4
            }
        }, res);
    }
    async categoriesNew(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogEdit', {
            user: this.admin,
            page: {
                title: 'Add Categories'
            },
            total: {
                faq: 1,
                blog: 2,
                users: 3,
                products: 4
            }
        }, res);
    }
    async categoriesEdit(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogEdit', {
            user: this.admin,
            page: {
                title: 'Edit Categories'
            },
            total: {
                faq: 1,
                blog: 2,
                users: 3,
                products: 4
            }
        }, res);
    }

    /**
     * Tags
     * @param {*} req 
     * @param {*} res 
     */
    async tags(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogManage', {
            user: this.admin,
            page: {
                title: 'Blog Tags',
                addNew: '/admin/cms/tags/new/'
            },
            total: {
                faq: 1,
                blog: 2,
                users: 3,
                products: 4
            }
        }, res);
    }
    async tagsNew(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogEdit', {
            user: this.admin,
            page: {
                title: 'Add Tag'
            },
            total: {
                faq: 1,
                blog: 2,
                users: 3,
                products: 4
            }
        }, res);
    }
    async tagsEdit(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/cms/blogEdit', {
            user: this.admin,
            page: {
                title: 'Edit Tags'
            },
            total: {
                faq: 1,
                blog: 2,
                users: 3,
                products: 4
            }
        }, res);
    }

    /**
     * Setting
     * @param {*} req 
     * @param {*} res 
     */
    async setting(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/dashboard', {
            user: this.admin,
            page: {
                title: 'Blog Setting'
            },
            total: {
                faq: 1,
                blog: 2,
                users: 3,
                products: 4
            }
        }, res);
    }
}

module.exports = Blog;