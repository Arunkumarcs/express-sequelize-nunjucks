const Controller = _use(`Controller/Var/Controller`);

// Model
let AdminModel = _use(`Model/AppAdmin`);
AdminModel = new AdminModel();

class Dashboard extends Controller {
    _init() {
        this._protected = _protected;
    }

    // Dashboard Paeg
    async index(req, res) {
        await this._loadAdmin(req, res);

        this._render('admin/dashboard', {
            user: this.admin,
            page: {
                title: 'Dashboard'
            },
            total: {
                faq: 1,
                blog: 2,
                users: 3,
                products: 4
            }
        }, res);
    }

    // Logout
    async logout(req, res) {
        _private.removeSession();
        res.redirect('/admin/login/');
    }

    async _loadAdmin(req, res) {
        await this._protected.getAdminFromSession(req, res);
    }

    /**
     * Getter and Setter
     */
    get admin() {
        return this._protected.admin;
    }
}

// Private Method
let _private = {
    removeSession(req) {
        req.session.admin = null;
    }
};
let _protected = {
    async getAdminFromSession(req, res) {
        this.admin = await AdminModel.boot(req.session.admin.id);
        return this.admin;
    }
};

module.exports = Dashboard;