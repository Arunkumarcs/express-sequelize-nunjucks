class _Server {
    constructor() {}

    // ServerResponse Events

    // Server Events
    Server_connection() { console.log('connection'); }
    Server_request() { console.log('request'); }
    Server_checkContinue() { console.log('checkContinue'); }
    Server_checkExpectation() { console.log('checkExpectation'); }
    Server_clientError() { console.log('clientError'); }
    Server_close() { console.log('close'); }
    Server_connect() { console.log('connect'); }
    Server_upgrade() { console.log('upgrade'); }
}

module.exports = (server) => {
    let event = new _Server(server);

    // ServerResponse Events

    // Server Events
    // server.on('connection', event.Server_connection);
    // server.on('request', event.Server_request);
    // server.on('checkContinue', event.Server_checkContinue);
    // server.on('checkExpectation', event.Server_checkExpectation);
    // server.on('clientError', event.Server_clientError);
    // server.on('close', event.Server_close);
    // server.on('connect', event.Server_connect);
    // server.on('upgrade', event.Server_upgrade);
};