module.exports = (io, app) => {
    app.io = io;

    var chat = io
        .of('/chat')
        .on('connection', function(socket) {
            chat.emit('news', { hello: 'world' })

            socket.on('myevent', function(name, fn) {
                fn({ data: "some random data", session: SESSION })
            });
        });
};