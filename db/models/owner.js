'use strict';
module.exports = (sequelize, DataTypes) => {
    var store_owner = sequelize.define('owner', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.BIGINT
        },
        name: DataTypes.STRING,
        email: DataTypes.STRING,
        password: DataTypes.STRING,
        key: DataTypes.STRING
    }, {
        indexes: [{
            unique: true,
            name: 'owner_email',
            fields: ['email']
        }, {
            name: 'owner_name',
            fields: ['name']
        }],
        classMethods: {
            associate: function(models) {
                // associations can be defined here
            }
        }
    });
    return store_owner;
};