'use strict';
module.exports = (sequelize, DataTypes) => {
    var product = sequelize.define('products', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.BIGINT
        },
        title: DataTypes.STRING,
        description: DataTypes.STRING,
        quantity: DataTypes.STRING,
        price: DataTypes.STRING,
        store_id: DataTypes.BIGINT,
        owner_id: DataTypes.BIGINT
    }, {
        indexes: [{
            name: 'products_id',
            fields: ['title', 'store_id', 'owner_id']
        }],
        classMethods: {
            associate: function(models) {
                // associations can be defined here
            }
        }
    });
    return product;
};