'use strict';
module.exports = (sequelize, DataTypes) => {
    var app_admin_options = sequelize.define('app_admin_options', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.BIGINT
        },
        admin_id: DataTypes.BIGINT,
        option_name: DataTypes.STRING,
        option_value: DataTypes.TEXT
    }, {
        indexes: [{
            name: 'admin_options_id',
            fields: ['option_name', 'admin_id']
        }],
        classMethods: {
            associate: function(models) {
                // associations can be defined here
            }
        }
    });
    return app_admin_options;
};