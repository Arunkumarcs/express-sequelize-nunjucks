'use strict';
module.exports = (sequelize, DataTypes) => {
    var store_owner_options = sequelize.define('owner_option', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.BIGINT
        },
        owner_id: DataTypes.BIGINT,
        option_name: DataTypes.STRING,
        option_value: DataTypes.TEXT
    }, {
        indexes: [{
            name: 'owner_option_id',
            fields: ['option_name', 'owner_id']
        }],
        classMethods: {
            associate: function(models) {
                // associations can be defined here
            }
        }
    });
    return store_owner_options;
};