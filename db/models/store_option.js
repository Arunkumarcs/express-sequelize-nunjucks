'use strict';
module.exports = (sequelize, DataTypes) => {
    var store_option = sequelize.define('store_option', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.BIGINT
        },
        option_name: DataTypes.STRING,
        option_value: DataTypes.TEXT,
        store_id: DataTypes.BIGINT,
        owner_id: DataTypes.BIGINT
    }, {
        indexes: [{
            name: 'store_option_id',
            fields: ['option_name', 'owner_id', 'store_id']
        }],
        classMethods: {
            associate: function(models) {
                // associations can be defined here
            }
        }
    });
    return store_option;
};