'use strict';
module.exports = (sequelize, DataTypes) => {
    var app_option = sequelize.define('app_option', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.BIGINT
        },
        option_name: DataTypes.STRING,
        option_value: DataTypes.TEXT
    }, {
        indexes: [{
            name: 'app_option_name',
            fields: ['option_name']
        }],
        classMethods: {
            associate: function(models) {
                // associations can be defined here
            }
        }
    });
    // app_option.sync().then(() => {
    //     _log('Table "app_option" created.');
    // }).catch(error => {
    //     _log('Table "app_option" not created.');
    // });

    return app_option;
};