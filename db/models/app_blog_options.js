'use strict';
module.exports = (sequelize, DataTypes) => {
    var app_blog_options = sequelize.define('app_blog_options', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.BIGINT
        },
        blog_id: DataTypes.BIGINT,
        option_name: DataTypes.STRING,
        option_value: DataTypes.TEXT
    }, {
        indexes: [{
            name: 'blog_options_id',
            fields: ['option_name', 'blog_id']
        }],
        classMethods: {
            associate: function(models) {
                // associations can be defined here
            }
        }
    });
    return app_blog_options;
};