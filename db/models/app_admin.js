'use strict';
module.exports = (sequelize, DataTypes) => {
    var app_admin = sequelize.define('app_admin', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.BIGINT
        },
        name: DataTypes.STRING,
        email: DataTypes.STRING,
        password: DataTypes.STRING,
        key: DataTypes.STRING
    }, {
        indexes: [{
            unique: true,
            name: 'app_admin_email',
            fields: ['email']
        }, {
            name: 'app_admin_name',
            fields: ['name']
        }],
        classMethods: {
            associate: function(models) {
                // associations can be defined here
            }
        }
    });
    return app_admin;
};