'use strict';
module.exports = (sequelize, DataTypes) => {
    var store = sequelize.define('store', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.BIGINT
        },
        name: DataTypes.STRING,
        owner_id: DataTypes.BIGINT
    }, {
        indexes: [{
            unique: true,
            name: 'store_id_name',
            fields: ['owner_id', 'name']
        }],
        classMethods: {
            associate: function(models) {
                // associations can be defined here
            }
        }
    });
    return store;
};