'use strict';
module.exports = (sequelize, DataTypes) => {
    var user_option = sequelize.define('user_option', {
        option_name: DataTypes.STRING,
        option_value: DataTypes.TEXT,
        store_id: DataTypes.BIGINT,
        owner_id: DataTypes.BIGINT,
        user_id: DataTypes.BIGINT
    }, {
        indexes: [{
            name: 'user_option_id',
            fields: ['option_name', 'owner_id', 'store_id', 'user_id']
        }],
        classMethods: {
            associate: function(models) {
                // associations can be defined here
            }
        }
    });
    return user_option;
};