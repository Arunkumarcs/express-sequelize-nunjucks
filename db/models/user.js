'use strict';
module.exports = (sequelize, DataTypes) => {
    var store_user = sequelize.define('user', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.BIGINT
        },
        name: DataTypes.STRING,
        email: DataTypes.STRING,
        password: DataTypes.STRING,
        key: DataTypes.STRING,
        store_id: DataTypes.BIGINT,
        owner_id: DataTypes.BIGINT
    }, {
        indexes: [{
            unique: true,
            name: 'user_email',
            fields: ['email', 'store_id']
        }, {
            name: 'user_id_name',
            fields: ['name', 'owner_id']
        }],
        classMethods: {
            associate: function(models) {
                // associations can be defined here
            }
        }
    });
    return store_user;
};