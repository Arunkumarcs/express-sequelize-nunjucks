'use strict';
module.exports = (sequelize, DataTypes) => {
    var app_blog = sequelize.define('app_blog', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.BIGINT
        },
        title: DataTypes.STRING,
        slug: DataTypes.STRING,
        content: DataTypes.TEXT,
        created_by: DataTypes.BIGINT,
        type: DataTypes.ENUM(
            'Page',
            'Post',
            'Images',
            'Category',
            'Tage',
            'Faq'
        )
    }, {
        indexes: [{
            unique: true,
            name: 'blog_title',
            fields: ['title', 'type']
        }, {
            unique: true,
            name: 'blog_slug',
            fields: ['slug', 'type']
        }, {
            name: 'blog_creator',
            fields: ['created_by']
        }],
        classMethods: {
            associate: function(models) {
                // associations can be defined here
            }
        }
    });
    return app_blog;
};