/*
 * Initialize Express.
 */
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
express = require('express');
const app = express();
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const nunjucks = require('nunjucks');
expressDebug = require('express-debug')

/*
 * view engine setup & Adding nunjucks as Template Engine.
 */
app.set('views', path.join(__dirname, 'resources/Views'));
app.set('view engine', 'jade');
let viewEnv = nunjucks.configure(path.join(__dirname, 'resources/Views'), {
    autoescape: true,
    express: app,
    watch: true
});


// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
if (process.env.NODE_ENV != 'production') {
    // app.use(logger('dev'));
    // app.use(logger('short'));
    // app.use(logger('tiny'));
}
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


/*
 * App configuration $config
 */
require('./app/Config/Config.js')(app, viewEnv);


module.exports = app;