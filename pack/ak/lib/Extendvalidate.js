$validate.isEmail = (email) => {
	let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
};

$validate.checkPassword = (password) => {
    let re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
    return re.test(password);
};

$validate.passwordStrength = (password) => {
    let strength = 0;
    strength    += /[A-Z]+/.test(password) ? 1 : 0;
    strength    += /[a-z]+/.test(password) ? 1 : 0;
    strength    += /[0-9]+/.test(password) ? 1 : 0;
    strength    += /[\W]+/.test(password) ? 1 : 0;

    switch (strength) {
        case 3:
            // its's medium!
            break;
        case 4:
            // it's strong!
            break;
        default:
            // it's weak!
            break;
    }
};
