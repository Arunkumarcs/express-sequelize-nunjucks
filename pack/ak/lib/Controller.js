const fs = require('fs');

class Controller {
    constructor() {
        this.__themeName = 'Default';
        this.__theme = {};
        this._loadThemeSetting();

        this._properties();
        this._init();
        // this._nuxtObj = $nuxt;
    }


    _properties() {}
    _init() {}


    /**-------------------------------------------------------------------------
     * Default Pages
     * $nuxt.render(req, res);
     */
    index(req, res) {
        let self = this;
        self._render('index', { _pageName: 'home' }, res);
    }

    _page404(res) {
        let self = this;
        res.status(404);
        self._render('404', {
            _pageName: '400-Page'
        }, res);
    }

    _page500(res) {
        let self = this;
        res.status(500);
        self._render('500', {
            _pageName: '500-Page'
        }, res);
    }


    /**-------------------------------------------------------------------------
     * Render Functiopns
     */
    // Render JSON
    _renderJ(data, res) {
        res.json(data);
    }

    // Render JSONP
    _renderJP(data, res) {
        res.jsonp(data);
    }

    // Render Page
    _render(file, obj, res) {
        let self = this;
        file = file + '.' + (self._theme['extension'] || 'njk');

        try {
            if (fs.existsSync($config.namespace.Views + self._theme['path'] + file)) {
                file = $config.namespace.Views + self._theme['path'] + file;
            } else
            if (fs.existsSync($config.namespace.Views + self.__theme['Default']['path'] + file)) {
                _log(`\n View not exist ` + self._theme['path'] + file + ` Loading from default theme.\n`);
                file = $config.namespace.Views + self.__theme['Default']['path'] + file;
            } else {
                _log(`\n View not exist ` + self._theme['path'] + file + ` Loading Error page`);
                throw "File Not exist.";
            }
            obj._assets = self._theme;
            obj._controller = self;
            res.render(file, obj);
        } catch (error) {
            obj._assets = self.__theme['Default'];
            obj._controller = self;
            self._page404(res);
        }
    }


    /**-------------------------------------------------------------------------
     * Getters
     */
    get _theme() {
        return this.__theme[this.__themeName];
    }

    /**-------------------------------------------------------------------------
     * Setters
     */
    set _theme(theme) {
        this.__themeName = theme;
    }


    /**-------------------------------------------------------------------------
     * Private Methods
     */
    _loadThemeSetting() {
        let jsonTheme = _use('Controller/Var/theme.json');

        for (let theme in jsonTheme) {
            if (fs.existsSync($config.namespace.Views + jsonTheme[theme])) {
                this.__theme[theme] = _use('Views/' + jsonTheme[theme]);
            } else {
                _log(`Theme "${theme}" not configured properly.\n`);
            }
        }
    }
}

module.exports = Controller;