/**
 * Namespace
 */
$config.BASE_PATH = process.cwd() + '/';
let appPath = $config.BASE_PATH + 'app/';
let nameSpace = require(appPath + 'Config/Namespace.json');
for (let variable in nameSpace) {
    nameSpace[variable] = $config.BASE_PATH + nameSpace[variable];
}
nameSpace['BASE'] = $config.BASE_PATH;
$config.namespace = nameSpace;


/**
 * Alternative to Require
 */
_use = (pack, ...par) => {
    for (let variable in $config.namespace) {
        if (pack.startsWith(variable + '/')) {
            pack = pack.replace(variable + '/', $config.namespace[variable]);
        }
    }
    if (par.length > 0) {
        return require(pack)(...par);
    } else {
        return require(pack);
    }
};


/**
 * Log
 */
_log = (data) => {
    if ($debug) { console.log(data); }
};


// Global packages.
$loadash = _use('lodash');
$async = _use('async');
$validate = _use('validate.js');
$moment = _use('moment');

require('./lib/Extendvalidate');
$Controller = require('./lib/Controller');
$Events = require('./lib/Events');