# AK Express Template
Express Framework with Nunjucks Template-Engine and Sequalize ORM.

## Global Packages

### npm installation

* npm install -g express-generator 
* npm install -g npm-run-all pm2
* npm install -g sequelize
* npm install -g sequelize-cli
* npm install -g mysql2 
* npm install -g pg pg-hstore 

### yarn Installation

* yarn global add express-generator 
* yarn global add npm-run-all 
* yarn global add pm2
* yarn global add sequelize
* yarn global add sequelize-cli
* yarn global add mysql2 
* yarn global add pg pg-hstore 

