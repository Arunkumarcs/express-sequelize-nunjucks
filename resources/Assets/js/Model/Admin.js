const Controller = require('../Lib/Controller');

class Admin extends Controller {
    /**
     * Login
     */
    login() {
        let self = this;
        self.arguments = [...arguments];
        self.e_triggerValidate(self);
    }

    e_triggerValidate(self) {
        $('body').on('click', 'form button', () => {
            let email = $('form input[name="email"]').val();
            let password = $('form input[name="password"]').val();

            $('.overlay').fadeIn();
            $Ak.ajax({
                url: '/admin/login/validate/',
                data: {
                    email,
                    password
                },
                success: (data) => {
                    if (data.status == 'success') {
                        $Ak.redirect('/admin/dashboard/');
                    } else {
                        $('.overlay').fadeOut();
                        _alert(data.message);
                    }
                },
                error: () => {
                    $('.overlay').fadeOut();
                }
            });
        });
        return self;
    }
}
module.exports = Admin;