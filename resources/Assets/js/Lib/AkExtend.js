// Ak Object
$Ak = window.$Ak = {
    /*
     * Variables
     */
    debug: true,
    page: {
        URL: document.URL,
        origin: document.location.origin,
        host: document.location.host,
        pathname: document.location.pathname
    },

    /**
     * Initialize
     */
    _init() {
        // CSRF
        $.ajaxSetup({
            data: {
                _csrf: $('meta[name="_csrf"]').attr('content')
            }
        });

        //Router
        this.page.pathname = this.page.pathname.split('/')
            .filter(function(params) {
                return params != '';
            }).join('/');
    },

    /*
     * Plugins
     */
    _loader: require('./Loader'),
    _validate: require('./Validate'),

    /*
     * ajaxSubmit from Ajax Form Plugin
     */
    submitForm(formId, options = {}) {
        let extObj = {
            dataType: 'json',
            async: true,
            type: 'POST',
            url: '/ajax/',
            data: {
                // class: '',
                // method: '',
                // args: {}
            },

            beforeSend: (jqXHR, settings) => {},
            success: (data, textStatus, jqXHR) => {},
            error: (jqXHR, textStatus, errorThrown) => {},
            complete: (jqXHR, textStatus) => {}
        };

        $.extend(true, extObj, options); // console.log(extObj);
        formId.ajaxSubmit(extObj);
    },

    // Ajax
    ajax(obj = {}) {
        let extObj = {
            dataType: 'json',
            async: true,
            type: 'POST',
            url: '/api/',
            data: {},

            beforeSend: (jqXHR, settings) => {},
            success: (data, textStatus, jqXHR) => {},
            error: (jqXHR, textStatus, errorThrown) => {},
            complete: (jqXHR, textStatus) => {}
        };

        $.extend(true, extObj, obj);
        return $.ajax(extObj);
    },

    // Redirect & Reload
    redirect(url) {
        window.location.href = url;
    },
    openTab(url) {
        window.open(url);
    },
    reload(url) {
        location.reload();
    },

    // Route
    routes(pattern, callback) {
        var patt = new RegExp('^' + pattern, 'i');

        if (patt.test(this.page.pathname)) {
            let params = this.page.pathname.split('/');
            callback(...params);
        }
    }
};

/*
 * Methods
 */
// Log
_log = (text) => {
    if ($Ak.debug == true) {
        console.log(text);
    }
};

// Alert
_alert = (text) => {
    if ($Ak.debug == true) {
        if (typeof swal == 'function') {
            swal(text);
        } else {
            alert(text);
        }
    }
};

// Initalize Method
$Ak._init();