// import Vue from 'vue'
// $Vue = window.$Vue = Vue;

require('./Lib/AkExtend');

// Jquery
$(function() {
    setTimeout($Ak._loader.hide, 1000);
    _log($Ak);

    $Ak.routes('admin', function() {
        let Admin = require('./Model/Admin');
        Admin = new Admin();

        /**
         * Activate Menu
         */
        if ($('.main-sidebar .sidebar-menu').length > 0) {
            if ($(`.main-sidebar .sidebar-menu a[href="/${$Ak.page.pathname}/"]`).length > 0) {
                $(`.main-sidebar .sidebar-menu a[href="/${$Ak.page.pathname}/"]`).addClass('active')
                    .parents('li')
                    .addClass('active')
                    .addClass('menu-open');
            }
        }

        $Ak.routes('admin/login', function() {
            Admin.login(...arguments);
        });
        $Ak.routes('admin/cms', function() {
            $Ak.routes('admin/cms/blog', function() {
                $Ak.routes('admin/cms/faq', function() {});
                $Ak.routes('admin/cms/pages', function() {});
                $Ak.routes('admin/cms/images', function() {});
                $Ak.routes('admin/cms/categories', function() {});
                $Ak.routes('admin/cms/tags', function() {});
                $Ak.routes('admin/cms/setting', function() {});
            });
        });
    });
});